package richtercloud.jpa.map.with.non.unique.value.set;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author richter
 */
@Named
@ApplicationScoped
public class Init implements Serializable {
    private static final long serialVersionUID = 1L;
    @EJB
    private InitService initService;

    public Init() {
    }

    public String init() {
        return initService.init();
    }
}
