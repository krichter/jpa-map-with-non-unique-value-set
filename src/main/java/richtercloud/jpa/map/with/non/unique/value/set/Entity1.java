package richtercloud.jpa.map.with.non.unique.value.set;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.CollectionTable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;

/**
 *
 * @author richter
 */
@Entity
public class Entity1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    @OneToMany
//    @MapKey(name = "id")
    @MapKeyColumn
    private Map<String, Entity2> entity2Map = new HashMap<>();

    public Entity1() {
    }

    public Entity1(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<String, Entity2> getEntity2Map() {
        return entity2Map;
    }

    public void setEntity2Map(Map<String, Entity2> entity2Map) {
        this.entity2Map = entity2Map;
    }
}
