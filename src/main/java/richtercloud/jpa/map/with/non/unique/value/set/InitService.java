package richtercloud.jpa.map.with.non.unique.value.set;

import javax.ejb.Local;

/**
 *
 * @author richter
 */
@Local
public interface InitService {

    String init();
}
