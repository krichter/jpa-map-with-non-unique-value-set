package richtercloud.jpa.map.with.non.unique.value.set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author richter
 */
@Stateless
public class DefaultInitService implements InitService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public String init() {
        Entity2 entity2 = new Entity2(1l);
        Entity1 entity1 = new Entity1(2l);
        entity1.getEntity2Map().put("a", entity2);
        entity1.getEntity2Map().put("b", entity2);
        entityManager.persist(entity2);
        entityManager.persist(entity1);
        return "Hello world!";
    }
}
